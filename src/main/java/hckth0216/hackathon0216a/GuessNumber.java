/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hckth0216.hackathon0216a;

import java.io.Serializable;
import java.util.Random;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author marcin
 */
@Named
@SessionScoped
public class GuessNumber implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer rndInt = null;
    private int max = 10;
    private int min = 0;

    public GuessNumber() {
        Random rnd = new Random();
        int range = max + min + 1;
        rndInt = (int) (min + rnd.nextDouble() * range);
    }

    public Integer getRndInt() {
        return rndInt;
    }

    public void setRndInt(Integer rndInt) {
        this.rndInt = rndInt;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }
    
}
