/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hckth0216.hackathon0216a;

import java.util.Date;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author marcin
 */

@Named("helloClass1")
@RequestScoped
public class HelloClass1 {
    
    private String name;
    private Date date1;
    private Integer myint;
    private String str1;
    
    public String submit(){
        if(name.length()<4)
            return "failure";
        else
            return "success";
    }

    public String getStr1() {
        return str1;
    }

    public void setStr1(String str1) {
        this.str1 = str1;
    }

    public Integer getMyint() {
        return myint;
    }

    public void setMyint(Integer myint) {
        this.myint = myint;
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
